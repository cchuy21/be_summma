# SummmaBE

Proyecto BackEnd de Summma

## Run

`npm run start`

## mysqlconf

es encesario el archivo de mysqlconf.js
```
const mysql = require('mysql');

const pool = mysql.createPool({
    host: "",
    user: "",
    password: "",
    database: ""

});

var getConnection = (callback) => {
    pool.getConnection(function(err, connection) {
        if (err) {
            console.error('error connecting: ' + err.stack);
            return;
        }
        callback(err, connection);
    });
}

module.exports = { getConnection };

```


### Nodemon necesario

`npm install nodemon -g`
