const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const https = require('https');
const path = require('path');
const dir = path.join(__dirname, 'public');
app.use(express.static(dir));
const db = require('./mysqlconf');
var ids = [];

server.listen(3000, function() {
    console.log('Servidor corriendo en http://localhost:3000');
});

io.on('connection', function(socket) {
    console.log('Un cliente se ha conectado');

    socket.on("disconnect", () => {
        console.log("desconectado");
    });

    socket.on("login", (user, pass) => {
        socket.emit("login");
    });

});